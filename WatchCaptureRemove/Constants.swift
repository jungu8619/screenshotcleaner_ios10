//
//  Constants.swift
//  ScreenshotCleaner
//
//  Created by JHJG on 2016. 7. 8..
//  Copyright © 2016년 KangJungu. All rights reserved.
//

import Foundation

struct Constants {
    static let BannerID = "ca-app-pub-2750504344883958/2136183428"
    static let EntirePageID = "ca-app-pub-2750504344883958/5089649825"
}
