//
//  PhotoHelper.swift
//  WatchCaptureRemove
//
//  Created by JHJG on 2016. 5. 8..
//  Copyright © 2016년 KangJungu. All rights reserved.
//

import Foundation
import Photos


class PhotoHelper {
    
    private var assets:Array<PHAsset>?
    private let IPHONE_TYPE = 0
    private let IPAD_TYPE = 1
    private let APPLEWATCH_TYPE = 2
    private let ALL_TYPE = 3
    
    private var width = [Int]()
    private var height = [Int]()
    var viewController: ViewController!
    
    
    init(view:ViewController){
        self.viewController = view
        assets = []
    }
    
    //type마다 사진 가져온다.
    func getAsset(_ type: Int) -> Array<PHAsset>?{
        
        assets?.removeAll()
        //applte watch 사진
        if type == APPLEWATCH_TYPE || type == ALL_TYPE{
            self.width.append(312)
            self.height.append(390)
        }
        
        if type == IPHONE_TYPE || type == ALL_TYPE{
            //iphone 6
            self.width.append(750)
            self.height.append(1334)
            
            self.width.append(1334)
            self.height.append(750)
            
            //iphone 6+
            self.width.append(1242)
            self.height.append(2208)
            
            self.width.append(2208)
            self.height.append(1242)
            
            //iphone 5
            self.width.append(640)
            self.height.append(1136)
            
            self.width.append(1136)
            self.height.append(640)
            
            //iphone 4s
            self.width.append(640)
            self.height.append(960)
            
            self.width.append(960)
            self.height.append(640)
        }
        
        if type == IPAD_TYPE || type == ALL_TYPE{
            //ipad and ipad mini
            self.width.append(1536)
            self.height.append(2048)
            
            self.width.append(2048)
            self.height.append(1536)
            
            //ipad 2 and ipad mini
            self.width.append(768)
            self.height.append(1024)
            
            self.width.append(1024)
            self.height.append(768)
            
            //ipad pro
            self.width.append(2048)
            self.height.append(2732)
            
            self.width.append(2732)
            self.height.append(2048)
        }
        
        let options = PHFetchOptions()
        options.predicate = Predicate(format: "mediaType = %d",PHAssetMediaType.image.rawValue)
        options.sortDescriptors = [ SortDescriptor(key: "creationDate", ascending: true) ]
        let result : PHFetchResult = PHAsset.fetchAssets(with: options)
        
        for i in 0 ..< result.count {
            let asset = result[i] 
            for j in 0 ..< width.count {
                if asset.pixelWidth == width[j] && asset.pixelHeight == height[j] {
                    assets?.append(asset)
                    
                }
            }
        }
        
        self.width.removeAll()
        self.height.removeAll()
        
        return assets
    }
    
    func photoDelete(_ remove:Dictionary<Int,PHAsset>){
        var removeArray = Array<PHAsset>()
        
        //Dictionary to Array
        for (_,value) in remove {
            removeArray.append(value)
        }
        
        let arrayToDelete = NSArray(array: removeArray)
        
        PHPhotoLibrary.shared().performChanges( {
            PHAssetChangeRequest.deleteAssets(arrayToDelete)},
                                                            completionHandler: {
                                                                success, error in
                                                                print("Finished deleting asset. %@", (success ? "Success" : error))
                                                                //UI Thread(메인 쓰레드)에서 일을 처리하도록함!
                                                                if success {
                                                                    DispatchQueue.main.sync(execute: {
                                                                        self.viewController.doClearAssets()
                                                                        self.viewController.doEnd(arrayToDelete.count)
                                                                    });
                                                                }
                                                                
                                                                
                                                                
                                                                
        })
        
    }
    
}
