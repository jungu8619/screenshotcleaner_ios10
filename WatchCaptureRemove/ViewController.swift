//
//  ViewController.swift
//  WatchCaptureRemove
//
//  Created by JHJG on 2016. 5. 8..
//  Copyright © 2016년 KangJungu. All rights reserved.
//
// 기본 광고 밑에 + 삭제하면 큰광고

import UIKit
import Photos
import GoogleMobileAds
private let reuseIdentifier = "Cell"

class ViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,GADInterstitialDelegate{
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var fetchOptions:PHFetchOptions?
    var assets:Array<PHAsset>?
    var selectAssets:Dictionary<Int,PHAsset>?
    var scale:CGFloat?
    var targetSizeX:CGFloat?
    let imageManager = PHImageManager()
    var alertController,popupController:UIAlertController?
    var allItemAction,appleWatchAction,iPhoneAction,iPadAction,cancelAction,removeAction:UIAlertAction?
    var admobInterstitial : GADInterstitial?
    
    private let IPHONE_TYPE = 0
    private let IPAD_TYPE = 1
    private let APPLEWATCH_TYPE = 2
    private let ALL_TYPE = 3
    var checkSelect:Dictionary<Int,Bool>?
    var selectAll:Bool = false;
    //인앱 여부
    var canRemoveAds:Bool = false
    
    var photoHelper:PhotoHelper?
    
    override func viewDidLoad() {
        //title 색상 변경
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white()]
        //광고 시작
        startBannerAd()
        //전체광고 set
        admobInterstitial = setEntireAd()
        
        //photo helper에 main viewController 넣음
        photoHelper = PhotoHelper.init(view: self)
        //dictonary 초기화
        checkSelect = Dictionary<Int,Bool>()
        
        //click item set
        setClickItem()
        
        //collectionView UX 설정
        collectionView.backgroundColor = UIColor.white()
        collectionView.delegate = self
        collectionView.dataSource = self
        //여러개 선택할수 있게해줌!!
        collectionView.allowsMultipleSelection = true
        
        //화면의 가로 사이즈를 구한다.
        //화면이 landsacpe라면 세로 사이즈를 구한다.
        scale = UIScreen.main().scale
        
        //사진이 생긴 순서대로 sort함
        fetchOptions = PHFetchOptions()
        fetchOptions!.sortDescriptors = [SortDescriptor(key:"creationDate", ascending: true)]
        
        reloadData()
        super.viewDidLoad()
    }
    
    //collection view에서 각 셀을 정의할 메소드 정의
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        cell.isUserInteractionEnabled = true
        
        //토탈선택한 아이템들을 바꿔준다.
        if let n = checkSelect?[(indexPath as NSIndexPath).row] {
            print("indexPath \((indexPath as NSIndexPath).row), selected : \(n)")
            cell.isSelected = n
            
            //이것들이 있어야지 collectionView가 cell이 변한지 알수 있다.
            if n == true{
                //변화한 경우
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition())
            }else{
                //변화하지 않은 경우
                collectionView.deselectItem(at: indexPath, animated: true)
            }
        }
        
        cell.imageManager = imageManager
        cell.imageAsset = assets![(indexPath as NSIndexPath).row]
        return cell
    }
    
    // 셀이 선택되었을 때를 설정하는 메소드
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! CollectionViewCell
        checkSelect![(indexPath as NSIndexPath).row] = true
        setSelectItem(true, key: (indexPath as NSIndexPath).row)
        print("select \(selectAssets?.count)")
    }
    
    //셀이 deslect 되었을 때를 설정
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        //        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! CollectionViewCell
        checkSelect![(indexPath as NSIndexPath).row] = false
        setSelectItem(false, key: (indexPath as NSIndexPath).row)
        print("deslect \(selectAssets?.count)")
    }
    
    
    //cell 갯수
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let n = assets?.count{
            return n
        }
        else{
            return 0
        }
        
    }
    
    //section 갯수
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // 3. 셀의 크기 설정이 이루어진다.
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // 사진앱과 가장 비슷한 UX를 제공하려면 minimumInteritemSpacingForSectionAtIndex, minimumLineSpacingForSectionAtIndex 둘 다 1로 설정하는 것이다.
        // 이 크기를 감안해서 Cell의 크기를 설정해 주어야 한다.
        // 만약 Spacing을 고려하지 않고 Cell 크기를 설정하게 되어 미묘하게 Cell 크기가 가로 크기를 넘길 경우 이쁘지 않은 레이아웃을 보게 될 것이다.
        // 그러므로 최종 Cell의 크기는 Spacing 값을 참조하여 빼주도록 한다.
        
        targetSizeX = self.collectionView.frame.width / 4 - 1 // Min Spacing For Cell
        return CGSize(width: targetSizeX!, height: targetSizeX!)
    }
    
    // 4. Cell 내부 아이템의 최소 스페이싱을 설정한다. 셀간의 가로 간격이라고 생각하면 된다.
    // 상세한 내역은 여기 참조 : https://developer.apple.com/library/ios/documentation/WindowsViews/Conceptual/CollectionViewPGforIOS/UsingtheFlowLayout/UsingtheFlowLayout.html
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1 as CGFloat
    }
    
    // 5. Cell 간 라인 스페이싱을 설정한다. 셀간의 세로 간격이라고 생각하면 된다.
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1 as CGFloat
    }
    
    //세로고정
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [.portrait, .portraitUpsideDown]
    }
    
    //click item init
    func setClickItem() {
        
        alertController = UIAlertController(
            title: nil,
            message: "Scan",
            preferredStyle: .alert)
        allItemAction = UIAlertAction(title: "All", style: .default) { (_) in
            //모든 캡쳐사진
            if let p = self.photoHelper{
                self.doClearAssets()
                self.assets = p.getAsset(self.ALL_TYPE)
                self.reloadData()
                self.title = "All"
                
            }
        }
        
        iPhoneAction = UIAlertAction(title: "iPhone", style: .default) { (_) in
            //iPhone에 대한것을 가져옴
            if let p = self.photoHelper{
                self.doClearAssets()
                self.assets = p.getAsset(self.IPHONE_TYPE)
                self.reloadData()
                self.title = "iPhone"
            }
        }
        
        iPadAction = UIAlertAction(title: "iPad", style: .default) { (_) in
            //iPad에 대한것을 가져옴
            if let p = self.photoHelper{
                self.doClearAssets()
                self.assets = p.getAsset(self.IPAD_TYPE)
                self.reloadData()
                self.title = "iPad"
            }
        }
        
        appleWatchAction = UIAlertAction(title: "Apple Watch", style: .default) { (_) in
            //apple watch에 대한것을 가져옴
            if let p = self.photoHelper{
                self.doClearAssets()
                self.assets = p.getAsset(self.APPLEWATCH_TYPE)
                self.reloadData()
                self.title = "Watch : \(self.assets!.count)"
            }
            
        }
        
        
        cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        if let tAlter = alertController {
            if allItemAction != nil{
                tAlter.addAction(allItemAction!)
                
            }
            if iPhoneAction != nil {
                tAlter.addAction(iPhoneAction!)
                
            }
            if iPadAction != nil {
                tAlter.addAction(iPadAction!)
                
            }
            if appleWatchAction != nil{
                tAlter.addAction(appleWatchAction!)
                
            }
            if cancelAction != nil{
                tAlter.addAction(cancelAction!)
            }
            
            
            self.present(
                tAlter,
                animated: true,
                completion: nil)
            
        }
        
    }
    
    //select item
    func setSelectItem(_ isSelect:Bool, key:Int) {
        if selectAssets == nil{
            selectAssets = Dictionary<Int,PHAsset>()
        }
        
        if isSelect {
            selectAssets?[key] = assets![key]
        }else{
            selectAssets?.removeValue(forKey: key)
        }
        
    }
    
    //click scan
    @IBAction func clickScan(_ sender: AnyObject) {
        if let tAlter = alertController {
            self.present(
                tAlter,
                animated: true,
                completion: nil)
        }
    }
    
    //click all select
    @IBAction func clickSelect(_ sender: UIBarButtonItem) {
        selectAll = !selectAll
        if let n = assets{
            for i in 0 ..< n.count {
                checkSelect![i] = selectAll
                setSelectItem(selectAll, key: i)
            }
        }
        
        reloadData()
    }
    
    //click remove
    @IBAction func clickRemove(_ sender: UIBarButtonItem) {
        if canRemoveAds == false{
            startEntireAd()
        }else{
            remove()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func remove(){
        if self.selectAssets?.count > 0{
            if let n = self.selectAssets{
                if let p = self.photoHelper{
                    p.photoDelete(n)
                }
            }
        }
    }
    
    //배열들에 있는 것들을 삭제
    func doClearAssets() {
        assets?.removeAll()
        selectAssets?.removeAll()
        self.checkSelect?.removeAll()
    }
    
    //reloadData후 배너를 제일 앞으로 다시 가져옴
    func reloadData(){
        collectionView.reloadData()
        view.bringSubview(toFront: bannerView)
    }
    
    //삭제 끝났을때
    func doEnd(_ count:Int) {
        reloadData()
        endPopup("remove Complete : \(count)", message:"" )
        self.title = ""
    }
    
    //팝업 띄움
    func endPopup(_ title:String,message:String){
        
        popupController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert)
        
        popupController?.addAction(cancelAction!)
        
        self.present(
            popupController!,
            animated: true,
            completion: nil)
        
    }
    
    //배너 광고 시작
    func startBannerAd(){
        //인앱 결제 한경우 이런식으로 광고 없앨수 있다.
        if canRemoveAds == false {
            let request : GADRequest = GADRequest()
            //시뮬레이터 인경우 이렇게 해줘야지 테스트 가능.
            request.testDevices = [kGADSimulatorID]
            bannerView.rootViewController = self
            bannerView.load(request)
            print("admop")
        } else{
            bannerView.removeFromSuperview()
        }
        
    }
    
    func setEntireAd() -> GADInterstitial{
        let interstitial = GADInterstitial(adUnitID: Constants.EntirePageID)
        let request : GADRequest = GADRequest()
        //시뮬레이터 인경우 이렇게 해줘야지 테스트 가능.
        request.testDevices = [kGADSimulatorID]
        interstitial.delegate = self
        interstitial.load(request)
        return interstitial
    }
    
    func startEntireAd(){
        if let isReady = admobInterstitial?.isReady {
            admobInterstitial?.present(fromRootViewController: self)
        }
    }
    
    func interstitial(_ ad: GADInterstitial!, didFailToReceiveAdWithError error: GADRequestError!) {
        print("interstitialDidFailToReceiveAdWithError:\(error.localizedDescription)")
        admobInterstitial = setEntireAd()
    }
    
    func interstitialDidDismissScreen(_ interstitial: GADInterstitial) {
        //x 눌렀을때 여기로 나옴
        print(#function+"end")
        remove()
    }
    
    
}
/**
 1. 전체 셀렉트 후 셀렉트 안되는 부분 o
 2. 아무것도 선택안하고 휴지통 누르면 리스트 날라가는 부분 o
 3. 이미지 갯수가 화면 리스트보다 많은 경우도 되는지 확인하기 o
 4. 삭제 다하고 팝업 띄워주기 o
 5. 가로모드 안되게 하기 (세모)
 6. 아이폰마 되게 하기 (세모)
 
 **/

