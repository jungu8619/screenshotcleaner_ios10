

import UIKit
import Photos

class CollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet var checkBox: CheckBox!
    
    var imageManager: PHImageManager?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    var imageAsset: PHAsset? {
        didSet {
            //서브뷰(여기서는 checkBox) 클릭 안되게 해주는것. 
            checkBox.isUserInteractionEnabled = false
            self.imageManager?.requestImage(for: imageAsset!, targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFill, options: nil) { image, info in
                self.imgView.image = image
            }
        }
    }
    

    override var isSelected: Bool {
        didSet {
            self.imgView.alpha = self.isSelected ? 0.5 : 1.0
            self.checkBox.isChecked = self.isSelected ? true : false
        }
    }
    
    
}
